<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\People;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($name)
    {
        $people = People::where('name', $name)->get();

        if ($people->isEmpty()) {
            return view('notfound');
        } else {

            return Inertia::render('Home', [
                'name' => $name,
                'comments' => Comment::with('people')->latest()->get()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $name)
    {
        // set validation
        $request->validate([
            'comment' => 'required|string|max:255'
        ]);

        // Find or create the People record by name
        $person = People::firstOrCreate(['name' => $name]);

        // create name
        $person->comments()->create([
            'comment' => $request->comment
        ]);

        return redirect('/wedding/' . $name . '#post4')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
