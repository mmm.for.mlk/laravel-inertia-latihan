<?php

namespace App\Http\Controllers;

use App\Models\People;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('people/Index', [
            'peoples' => People::latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('people/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // set validation
        $request->validate([
            'name' => 'required|string|max:20'
        ]);

        // create name
        People::create([
            'name' => $request->name
        ]);

        return redirect('/people')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(People $people)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(People $people, Request $request)
    {
        // dd($people->find($request->id));
        return Inertia::render('people/Edit', [
            'people' => $people->find($request->id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, People $people)
    {
        // set validation
        $request->validate([
            'name' => 'required|string|max:20'
        ]);

        $people = People::find($request->id);

        $people->update([
            'name' => $request->name
        ]);


        return redirect('/people')->with('success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $people = People::find($id);

        $people->comments()->delete();

        $people->delete();

        return redirect('/people')->with('success', 'Data Berhasil Dihapus');
    }
}
