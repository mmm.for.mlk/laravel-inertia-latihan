<?php

namespace Database\Factories;

use App\Models\Peoples;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PeopleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    // protected $model = Peoples::class;

    public function definition(): array
    {
        return [
            'name' => fake()->firstName()
        ];
    }
}
