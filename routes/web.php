<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\PeopleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Home');
// });

Route::get('/wedding/{name}', [CommentController::class, 'index']);
Route::post('/wedding/{name}', [CommentController::class, 'store']);
Route::get('/people', [PeopleController::class, 'index']);
Route::get('/people/create', [PeopleController::class, 'create']);
Route::post('/people', [PeopleController::class, 'store']);
Route::get('/people/edit/{id}', [PeopleController::class, 'edit']);
Route::put('/people/update/{id}', [PeopleController::class, 'update']);
Route::delete('/people/{id}', [PeopleController::class, 'destroy']);
