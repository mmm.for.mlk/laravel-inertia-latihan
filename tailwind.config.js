/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.jsx",
    "./resources/**/*.vue",
  ],
  theme: {
    extend: {
      backgroundColor: {
        'latar': '#DED0B6',
        'count': '#B2A59B',
      }
    },
  },
  plugins: [
    require("daisyui"),
    require('tailwind-scrollbar-hide'),
    require('tailwind-scroll-behavior')(), // no options to configure
  ],
}

