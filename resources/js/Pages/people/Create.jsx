import React, { useState } from "react";
import { router } from "@inertiajs/react";

export default function Create(props) {
    const [values, setValues] = useState({
        name: "",
    });

    function handleChange(e) {
        const key = e.target.id;
        const value = e.target.value;
        setValues((values) => ({
            ...values,
            [key]: value,
        }));
    }

    function handleSubmit(e) {
        e.preventDefault();
        router.post("/people", values);
    }

    return (
        <div className="flex items-center justify-center h-screen">
            <div className="max-w-md w-full mx-4 my-8 p-8 bg-white rounded-md shadow-md">
                <h1 className="text-2xl font-bold mb-4">Input Nama Undangan</h1>

                <form onSubmit={handleSubmit} className="space-y-4">
                    <label
                        htmlFor="name"
                        className="block text-sm font-medium text-gray-600"
                    >
                        Nama:
                    </label>
                    <input
                        type="text"
                        id="name"
                        value={values.name}
                        onChange={handleChange}
                        required
                        className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                    />
                    <button
                        type="submit"
                        className="w-full bg-blue-500 text-white py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600"
                    >
                        Submit
                    </button>
                </form>
            </div>
        </div>
    );
}
