import React from "react";
import { Link } from "@inertiajs/react";
import CopyToClipboard from "react-copy-to-clipboard";

export default function Index(props) {
    // console.log("isi props: ", props.peoples);
    const hostname = window.location.hostname;

    return (
        <div className="container mx-auto my-8">
            <Link
                method="get"
                as="button"
                href="/people/create"
                className="bg-blue-500 text-white py-2 px-4 rounded-md mb-4"
            >
                Tambah Undangan
            </Link>
            <div className="overflow-x-auto mt-2">
                <table className="table">
                    {/* head */}
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.peoples.map((peoples, index) => (
                            <tr key={index}>
                                <th>{index + 1}</th>
                                <td>{peoples.name}</td>
                                <td>
                                    <Link
                                        method="get"
                                        as="button"
                                        className="btn btn-outline btn-info mr-1"
                                        href={`/wedding/${peoples.name}`}
                                    >
                                        Lihat Undangan
                                    </Link>
                                    <CopyToClipboard
                                        text={`http://${hostname}/wedding/${peoples.name}`}
                                    >
                                        <button className="btn btn-outline btn-info mr-1">
                                            Salin Undangan
                                        </button>
                                    </CopyToClipboard>
                                    <Link
                                        as="button"
                                        method="get"
                                        href={`/people/edit/${peoples.id}`}
                                        className="btn btn-outline btn-warning mr-1"
                                    >
                                        Edit
                                    </Link>
                                    <Link
                                        as="button"
                                        method="delete"
                                        className="btn btn-outline btn-error mr-1"
                                        href={`/people/${peoples.id}`}
                                        data={peoples.id}
                                    >
                                        Delete
                                    </Link>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
