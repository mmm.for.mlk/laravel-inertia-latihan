import React from "react";
import Header from "./components/Header";
import Feed from "./components/Feed";
import Footer from "./components/Footer";

const Home = (props) => {
    // console.log("data :", props);
    return (
        <div className="relative text-black bg-slate-100">
            <Header />
            <div className="mt-12">
                <Feed comment={props} />
            </div>
            <Footer name={props.name} />
        </div>
    );
};

export default Home;
