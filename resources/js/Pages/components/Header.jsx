import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookMessenger } from "@fortawesome/free-brands-svg-icons";
import { faHeart } from "@fortawesome/free-regular-svg-icons";

export default function Header() {
  return(
    <div className="shadow-sm border-b bg-white top-0 fixed w-full z-20">
      
      <div className="flex justify-between max-w-4xl mx-4 xl:mx-auto p-2">
        <div className="flex justify-between w-4/6 items-center">
          
          <div className="relative lg:inline-grid">
            <img src="/images/Wedding-Invitation.svg" layout="fill" />
          </div>
        </div>
        <div className="flex items-center justify-end space-x-4">
            <div className="relative">
              <FontAwesomeIcon className="cursor-pointer" icon={faHeart} size="lg" />
              <div className="absolute w-1 h-1 left-1/2 -translate-x-1/2 bg-red-600 rounded-full"/>
            </div>
            <div className="relative">
              <FontAwesomeIcon className="cursor-pointer" icon={faFacebookMessenger} size="lg" />
              <span className="absolute -top-1 -right-2 bg-red-500 rounded-full h-4 w-4 text-white text-xs items-center justify-center flex leading-none">
                3
              </span>
            </div>
          </div>
      </div>
    </div>
  )
}