import React from "react";
import Stories from "./stories/Stories";
import Posts from "./posts/Posts";

export default function Feed({ comment }) {
    // console.log(comment);

    return (
        <div className="grid grid-cols-1 md:grid-cols-2 md:max-w-3xl xl:grid-cols-3 xl:max-w-4xl mx-auto">
            <section className="col-span-3 md:col-span-2">
                <div id="post1">
                    <Stories />
                </div>
                <Posts comment={comment} />
            </section>
        </div>
    );
}
