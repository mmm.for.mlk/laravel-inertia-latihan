import React, { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faAdd,
    faHome,
    faMusic,
    faSearch,
    faPause,
    faComment,
} from "@fortawesome/free-solid-svg-icons";
import { useSpring, animated } from "@react-spring/web";
import { faPaperPlane } from "@fortawesome/free-regular-svg-icons";
import { Link } from "react-scroll";

export default function Footer({ name }) {
    const audioRef = useRef(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [heroVisible, setHeroVisible] = useState(true);
    const [clickedIcon, setClickedIcon] = useState(null);

    const togglePlayPause = () => {
        if (isPlaying) {
            audioRef.current.pause();
        } else {
            audioRef.current.play().catch((error) => {
                console.error("Autoplay Failed:", error);
            });
        }

        setIsPlaying(!isPlaying);
    };

    const heroAnimation = useSpring({
        top: heroVisible ? "0%" : "-100%",
    });

    const playAudio = () => {
        audioRef.current
            .play()
            .then(() => {
                setIsPlaying(true);
            })
            .catch((error) => {
                console.error("Autoplay Failed:", error);
            });
    };

    const handleButtonClick = () => {
        setHeroVisible(false);
        playAudio();
    };

    const handleIconClick = (icon) => {
        setClickedIcon(icon);
    };

    useEffect(() => {
        const handleInteraction = () => {
            document.removeEventListener("click", handleInteraction);
            setHeroVisible(false);
            playAudio();
        };

        document.addEventListener("click", handleInteraction);

        return () => {
            document.removeEventListener("click", handleInteraction);
        };
    }, []);

    useEffect(() => {
        audioRef.current.volume = 0.4;
    }, []);

    return (
        <div>
            {/* hero section awal */}
            <animated.div
                className="fixed w-full h-full z-50 bg-cover bg-center flex items-center justify-center"
                style={{
                    ...heroAnimation,
                    backgroundImage: "url('/images/feeds/Rectangle 1.png')",
                }}
            >
                <div className="absolute top-10 right-12 flex items-center">
                    <p className="text-white ml-2">DM- Denisa & Mulki</p>
                    <FontAwesomeIcon
                        className="h-12 ml-4"
                        icon={faPaperPlane}
                        color="white"
                    />
                </div>

                <div className="text-white text-center mx-auto">
                    <img
                        className="mx-auto w-60"
                        src="/images/feeds/Wedding Invitation.png"
                    />
                    <img
                        className="mx-auto w-32 mt-4"
                        src="/images/feeds/Denisa & Mulki.png"
                    />
                    <img
                        className="mx-auto w-20 mt-8"
                        src="/images/feeds/Group 2.png"
                    />
                    <p className="mt-20 text-xl">for :</p>
                    <p className="mx-auto font-bold text-2xl">{name}</p>
                    <button
                        className="mt-8 bg-blue-500 text-white px-16 py-2 rounded-md"
                        onClick={handleButtonClick}
                    >
                        Log In
                    </button>
                </div>
            </animated.div>

            {/* menu footer */}
            <div className="shadow-md border-b bg-white bottom-0 fixed w-full z-20">
                <div className="flex justify-between max-w-4xl mx-4 xl:mx-auto p-4">
                    <Link
                        to="post1"
                        spy={true}
                        smooth={true}
                        duration={2000}
                        onClick={() => handleIconClick("home")}
                        className={`cursor-pointer ${
                            clickedIcon === "home" ? "text-blue-500" : ""
                        }`}
                    >
                        <FontAwesomeIcon
                            className="cursor-pointer"
                            icon={faHome}
                            size="lg"
                        />
                    </Link>

                    <Link
                        to="post2"
                        spy={true}
                        smooth={true}
                        duration={2000}
                        onClick={() => handleIconClick("search")}
                        className={`cursor-pointer ${
                            clickedIcon === "search" ? "text-blue-500" : ""
                        }`}
                    >
                        <FontAwesomeIcon
                            className="cursor-pointer"
                            icon={faSearch}
                            size="lg"
                        />
                    </Link>

                    <Link
                        to="post3"
                        spy={true}
                        smooth={true}
                        duration={2000}
                        onClick={() => handleIconClick("add")}
                        className={`cursor-pointer ${
                            clickedIcon === "add" ? "text-blue-500" : ""
                        }`}
                    >
                        <FontAwesomeIcon
                            className="cursor-pointer"
                            icon={faAdd}
                            size="lg"
                        />
                    </Link>

                    <Link
                        to="post4"
                        spy={true}
                        smooth={true}
                        duration={2000}
                        onClick={() => handleIconClick("comment")}
                        className={`cursor-pointer ${
                            clickedIcon === "comment" ? "text-blue-500" : ""
                        }`}
                    >
                        <FontAwesomeIcon
                            className="cursor-pointer"
                            icon={faComment}
                            size="lg"
                        />
                    </Link>

                    <FontAwesomeIcon
                        className="cursor-pointer"
                        icon={isPlaying ? faPause : faMusic}
                        size="lg"
                        onClick={togglePlayPause}
                    />

                    <audio
                        ref={audioRef}
                        src="/sounds/Laufey.mp3"
                        autoPlay
                        loop
                    />
                </div>
            </div>
        </div>
    );
}
