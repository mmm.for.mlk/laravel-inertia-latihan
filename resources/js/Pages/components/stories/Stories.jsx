import React, { useRef, useState } from "react";
import stories from './stories.json'
import Story from "./Story";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleLeft, faChevronCircleRight, faL } from "@fortawesome/free-solid-svg-icons";

export default function Stories() {
  const storiesRef = useRef(null);
  const [showLeft, setShowLeft] = useState(false);
  const [showRight, setShowRight] = useState(false);
  const onScroll = () => {
    if(storiesRef.current.scrollLeft>0) {
      setShowLeft(true)
    }else {
      setShowLeft(false)
    } 
  }
  
  return(
    <div className="relative">
      <div onScroll={onScroll} ref={storiesRef} className="flex space-x-2 overflow-x-scroll bg-white border-gray-200 p-4 scroll-smooth scrollbar-hide">
        {stories.map(story => 
          <Story
            key={story.id}
            img={story.images}
            username={story.nama}
          />)}
      </div>
      <div className="absolute top-0 p-4 w-full h-full flex justify-between z-10 items-center">
        <button onClick={()=> {storiesRef.current.scrollLeft=storiesRef.current.scrollLeft-200}}>
          <FontAwesomeIcon size="lg" icon={faChevronCircleLeft} color="white" className={`cursor-pointer filter  drop-shadow-lg ${showLeft?'visible':'invisible'}`}/>
        </button>
        <button onClick={()=> {storiesRef.current.scrollLeft=storiesRef.current.scrollLeft+200}}>
          <FontAwesomeIcon size="lg" icon={faChevronCircleRight} color="white" className={`cursor-pointer filter drop-shadow-lg ${showRight?'invisible':'visible'}`}/>
        </button>
      </div>
    </div>
  )
}