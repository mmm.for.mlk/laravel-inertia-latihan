import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import {
    faPaperPlane,
    faHeart,
    faComment,
    faBookmark,
} from "@fortawesome/free-regular-svg-icons";

export default function Post_6() {
    return (
        <div className="bg-white my-4 border rounded-sm">
            {/* top, image profile, name, logo */}
            <div className="flex items-center p-3">
                <div className="avatar mr-3">
                    <div className="rounded-full w-10 h-10 cursor-pointer">
                        <img src="/images/feeds/feed 2.jpg" />
                    </div>
                </div>
                <p className="flex-1 text-sm font-semibold cursor-pointer">
                    DM_Wedding
                </p>
                <FontAwesomeIcon
                    icon={faEllipsisVertical}
                    className="h-5 mr-2 cursor-pointer"
                />
            </div>

            {/* map content */}
            <iframe
                className="w-full"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11201.200103168867!2d107.56055191633709!3d-6.983075524409761!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68ef410847bc3f%3A0xd709ff419fc80c!2sGedung%20Omega%20Depohar%20Sulaiman!5e0!3m2!1sid!2sid!4v1704266336184!5m2!1sid!2sid"
                title="Google Maps Location"
                allowFullScreen=""
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
                height={500}
            ></iframe>

            {/* logos */}
            <div className="flex justify-between p-4">
                <div className="flex space-x-4 items-center">
                    <FontAwesomeIcon
                        icon={faHeart}
                        className="cursor-pointer"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faComment}
                        className="cursor-pointer scale-x-[-1]"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faPaperPlane}
                        className="cursor-pointer"
                        size="lg"
                    />
                </div>
                <FontAwesomeIcon
                    icon={faBookmark}
                    className="cursor-pointer"
                    size="lg"
                />
            </div>

            {/* Likes, name */}
            <div className="px-4">
                <p className="font-semibold mb-1 text-sm">354 Likes</p>
                <p className="mr-1 text-sm">
                    <span className="font-bold cursor-pointer">
                        DM_Wedding{" "}
                    </span>
                    Save the Location. Gedung Omega Sulaiman.
                </p>
            </div>

            {/* Comments, view all comment */}
            {/* <div className="px-4 flex justify-between mt-2">
        <div className="text-sm">
          <span className="font-semibold mr-1">afifarifmustofa</span>
          Baru tau masmul bisa berpose di kamera😁
        </div>
        <FontAwesomeIcon icon={faHeart} className="h-4 cursor-pointer" />
      </div>
      <div className="px-4 flex justify-between">
        <div className="text-sm">
          <span className="font-semibold mr-1">_emirbandu22</span>
          wih, pake apa mas Stack undangannya?
        </div>
        <FontAwesomeIcon icon={faHeart} className="h-4 cursor-pointer" />
      </div> */}

            <div className="px-4 text-sm mb-1 text-gray-400 cursor-pointer">
                313 comments
            </div>

            {/* Time */}
            <div className="px-4 mt-2 text-gray-400 text-xs mb-4">
                2 DAYS AGO
            </div>
        </div>
    );
}
