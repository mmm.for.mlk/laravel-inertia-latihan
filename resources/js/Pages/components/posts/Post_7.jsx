import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import {
    faPaperPlane,
    faHeart,
    faComment,
    faBookmark,
} from "@fortawesome/free-regular-svg-icons";
import {
    faChevronCircleLeft,
    faChevronCircleRight,
} from "@fortawesome/free-solid-svg-icons";

export default function Post_7() {
    const [curr, setCurr] = useState(0);

    const slides = [
        "/images/carrousel/carroussel 1a.jpg",
        "/images/carrousel/carroussel 1b.jpg",
        "/images/carrousel/carroussel 1c.jpg",
    ];

    const prev = () =>
        setCurr((curr) => (curr === 0 ? slides.length - 1 : curr - 1));
    const next = () =>
        setCurr((curr) => (curr === slides.length - 1 ? 0 : curr + 1));

    return (
        <div className="bg-white my-4 border rounded-sm">
            {/* top, image profile, name, logo */}
            <div className="flex items-center p-3">
                <div className="avatar mr-3">
                    <div className="rounded-full w-10 h-10 cursor-pointer">
                        <img src="/images/feeds/feed 2.jpg" />
                    </div>
                </div>
                <p className="flex-1 text-sm font-semibold cursor-pointer">
                    DM_Wedding
                </p>
                <FontAwesomeIcon
                    icon={faEllipsisVertical}
                    className="h-5 mr-2 cursor-pointer"
                />
            </div>

            {/* carousel content */}
            <div className="w-full overflow-hidden relative">
                <div
                    className="flex transition-transform ease-out duration-500"
                    style={{ transform: `translateX(-${curr * 100}%)` }}
                >
                    {slides.map((slide, index) => (
                        <img key={index} src={slide} className="w-full" />
                    ))}
                </div>

                {/* button prev next */}
                <div className="absolute inset-0 flex items-center justify-between p-4">
                    <button
                        onClick={prev}
                        className="p-1 rounded-full shadow text-gray-800 hover:bg-gray-400 "
                    >
                        <FontAwesomeIcon
                            icon={faChevronCircleLeft}
                            color="white"
                            size="2xl"
                        />
                    </button>
                    <button
                        onClick={next}
                        className="p-1 rounded-full shadow text-gray-800 hover:bg-gray-400 "
                    >
                        <FontAwesomeIcon
                            icon={faChevronCircleRight}
                            color="white"
                            size="2xl"
                        />
                    </button>
                </div>

                {/* slide circle */}
                <div className="absolute bottom-4 right-0 left-0">
                    <div className="flex items-center justify-center gap-2">
                        {slides.map((_, i) => (
                            <div
                                key={_}
                                className={`
                transition-all w-3 h-3 bg-white rounded-full
                ${curr === i ? "p-1" : "bg-opacity-75"}
              `}
                            />
                        ))}
                    </div>
                </div>
            </div>

            {/* logos */}
            <div className="flex justify-between p-4">
                <div className="flex space-x-4 items-center">
                    <FontAwesomeIcon
                        icon={faHeart}
                        className="cursor-pointer"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faComment}
                        className="cursor-pointer scale-x-[-1]"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faPaperPlane}
                        className="cursor-pointer"
                        size="lg"
                    />
                </div>
                <FontAwesomeIcon
                    icon={faBookmark}
                    className="cursor-pointer"
                    size="lg"
                />
            </div>

            {/* Likes, name */}
            <div className="px-4">
                <p className="font-semibold mb-1 text-sm">354 Likes</p>
                <p className="mr-1 text-sm">
                    <span className="font-bold cursor-pointer">
                        DM_Wedding{" "}
                    </span>
                    Our Slide Gallery~~~.
                </p>
            </div>

            {/* Comments, view all comment */}
            <div className="px-4 flex justify-between mt-2">
                <div className="text-sm">
                    <span className="font-semibold mr-1">kayfalab</span>
                    Salfok di slide 3 sama perut masmul😁
                </div>
                <FontAwesomeIcon
                    icon={faHeart}
                    className="h-4 cursor-pointer"
                />
            </div>
            <div className="px-4 flex justify-between">
                <div className="text-sm">
                    <span className="font-semibold mr-1">jujun_j94</span>
                    Kiranteh saya doang yang salfok sama slide 3 😂
                </div>
                <FontAwesomeIcon
                    icon={faHeart}
                    className="h-4 cursor-pointer"
                />
            </div>

            <div className="px-4 text-sm mb-1 text-gray-400 cursor-pointer">
                313 comments
            </div>

            {/* Time */}
            <div className="px-4 mt-2 text-gray-400 text-xs mb-4">
                2 DAYS AGO
            </div>
        </div>
    );
}
