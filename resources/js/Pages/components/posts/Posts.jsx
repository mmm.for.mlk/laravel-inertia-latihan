import React from "react";
import Post_1 from "./Post_1";
import Post_2 from "./Post_2";
import Post_3 from "./Post_3";
import Post_4 from "./Post_4";
import Post_5 from "./Post_5";
import Post_6 from "./Post_6";
import Post_7 from "./Post_7";
import Post_8 from "./Post_8";
import Post_9 from "./Post_9";
import Post_10 from "./Post_10";
import Footerpost from "./Footerpost";

export default function Posts({comment}) {
  // console.log(comment);

  return(
    <div>
      <Post_1 />
      <Post_2 />
      <Post_3 />
      <div id="post2">
        <Post_4 />
      </div>    
      <Post_5 />
        <Post_6 />
      <div id="post3">
        <Post_7 />
      </div>
      <Post_8 />
      <Post_9 />
      <div id="post4">
        <Post_10 comment={comment}  />
      </div>
      <Footerpost />
    </div>
  )
}