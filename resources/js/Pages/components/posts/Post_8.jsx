import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import {
    faPaperPlane,
    faHeart,
    faComment,
    faBookmark,
} from "@fortawesome/free-regular-svg-icons";

export default function Post_8() {
    const [days, setDays] = useState(0);
    const [hours, setHours] = useState(0);
    const [minutes, setMinutes] = useState(0);
    const [seconds, setSeconds] = useState(0);

    const newDate = new Date("2024-02-11").getTime();

    useEffect(() => {
        const timerId = setInterval(() => {
            const now = new Date().getTime();
            const distance = (newDate - now) / 1000;
            if (distance > 0) {
                const days = Math.floor(distance / 60 / 60 / 24);
                setDays(days);

                const hours = Math.floor((distance / 60 / 60) % 24);
                setHours(hours);

                const minutes = Math.floor((distance / 60) % 60);
                setMinutes(minutes);

                const seconds = Math.floor(distance % 60);
                setSeconds(seconds);
            } else {
                clearInterval(timerId);
            }
        }, 1000);
        return () => clearInterval(timerId);
    }, [newDate]);

    return (
        <div className="bg-white my-4 border rounded-sm">
            {/* top, image profile, name, logo */}
            <div className="flex items-center p-3">
                <div className="avatar mr-3">
                    <div className="rounded-full w-10 h-10 cursor-pointer">
                        <img src="/images/feeds/feed 2.jpg" />
                    </div>
                </div>
                <p className="flex-1 text-sm font-semibold cursor-pointer">
                    DM_Wedding
                </p>
                <FontAwesomeIcon
                    icon={faEllipsisVertical}
                    className="h-5 mr-2 cursor-pointer"
                />
            </div>

            {/* countdown timer content */}
            <div className="grid grid-cols-1 gap-5 text-center justify-center auto-cols-max p-5 grid-auto-flow-row bg-latar">
                <div className="flex flex-col p-2 bg-count rounded-box text-white">
                    <span className="countdown font-mono text-5xl items-center justify-center">
                        {days}
                    </span>
                    days
                </div>
                <div className="flex flex-col p-2 bg-count rounded-box text-white">
                    <span className="countdown font-mono text-5xl items-center justify-center">
                        {hours}
                    </span>
                    hours
                </div>
                <div className="flex flex-col p-2 bg-count rounded-box text-white">
                    <span className="countdown font-mono text-5xl items-center justify-center">
                        {minutes}
                    </span>
                    min
                </div>
                <div className="flex flex-col p-2 bg-count rounded-box text-white">
                    <span className="countdown font-mono text-5xl text-center justify-center">
                        {seconds}
                    </span>
                    sec
                </div>
            </div>

            {/* logos */}
            <div className="flex justify-between p-4">
                <div className="flex space-x-4 items-center">
                    <FontAwesomeIcon
                        icon={faHeart}
                        className="cursor-pointer"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faComment}
                        className="cursor-pointer scale-x-[-1]"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faPaperPlane}
                        className="cursor-pointer"
                        size="lg"
                    />
                </div>
                <FontAwesomeIcon
                    icon={faBookmark}
                    className="cursor-pointer"
                    size="lg"
                />
            </div>

            {/* Likes, name */}
            <div className="px-4">
                <p className="font-semibold mb-1 text-sm">354 Likes</p>
                <p className="mr-1 text-sm">
                    <span className="font-bold cursor-pointer">
                        DM_Wedding{" "}
                    </span>
                    We hope you can come.
                </p>
            </div>

            {/* Comments, view all comment */}
            {/* <div className="px-4 flex justify-between mt-2">
        <div className="text-sm">
          <span className="font-semibold mr-1">kayfalab</span>
          Salfok di slide 3 sama perut masmul😁
        </div>
        <FontAwesomeIcon icon={faHeart} className="h-4 cursor-pointer" />
      </div>
      <div className="px-4 flex justify-between">
        <div className="text-sm">
          <span className="font-semibold mr-1">jujun_j94</span>
          Kiranteh saya doang yang salfok sama slide 3 😂
        </div>
        <FontAwesomeIcon icon={faHeart} className="h-4 cursor-pointer" />
      </div> */}

            <div className="px-4 text-sm mb-1 text-gray-400 cursor-pointer">
                313 comments
            </div>

            {/* Time */}
            <div className="px-4 mt-2 text-gray-400 text-xs mb-4">
                2 DAYS AGO
            </div>
        </div>
    );
}
