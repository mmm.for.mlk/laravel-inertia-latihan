import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import {
    faPaperPlane,
    faHeart,
    faComment,
    faBookmark,
    faCopy,
} from "@fortawesome/free-regular-svg-icons";
import CopyToClipboard from "react-copy-to-clipboard";

export default function Post_9() {
    const [clipboardState, setClipboardState] = useState(false);

    const handleCopy = () => {
        setClipboardState(true);
        setTimeout(() => {
            setClipboardState(false);
        }, 1000); // Setelah 3 detik, reset clipboardState
    };

    return (
        <div className="bg-white my-4 border rounded-sm">
            {/* top, image profile, name, logo */}
            <div className="flex items-center p-3">
                <div className="avatar mr-3">
                    <div className="rounded-full w-10 h-10 cursor-pointer">
                        <img src="/images/feeds/feed 2.jpg" />
                    </div>
                </div>
                <p className="flex-1 text-sm font-semibold cursor-pointer">
                    DM_Wedding
                </p>
                <FontAwesomeIcon
                    icon={faEllipsisVertical}
                    className="h-5 mr-2 cursor-pointer"
                />
            </div>

            {/* image content */}
            <div className="text-center bg-latar flex items-center justify-center">
                <div className="p-14">
                    <span>
                        We are truly honored to have you celebrate this joyous
                        occasion with us. Your presence is the greatest gift we
                        could ask for.
                        <br />
                        <br />
                        For those wishing to make a non-cash gift, we have
                        provided our bank details below :
                        <br />
                        <br />
                        BSI (Bank Syari'ah Indonesia) <br />
                        7196772997 &nbsp;
                    </span>
                    <CopyToClipboard text="7196772997" onCopy={handleCopy}>
                        <button className="cursor-pointer">
                            {!clipboardState ? (
                                <FontAwesomeIcon icon={faCopy} />
                            ) : (
                                <FontAwesomeIcon icon={faCopy} color="white" />
                            )}
                        </button>
                    </CopyToClipboard>
                    <br />
                    <span>
                        Mulki Zulkarnaen Nurfalah
                        <br />
                        <br />
                        Thank you for being a part of our special day!
                    </span>
                </div>
            </div>

            {/* logos */}
            <div className="flex justify-between p-4">
                <div className="flex space-x-4 items-center">
                    <FontAwesomeIcon
                        icon={faHeart}
                        className="cursor-pointer"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faComment}
                        className="cursor-pointer scale-x-[-1]"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faPaperPlane}
                        className="cursor-pointer"
                        size="lg"
                    />
                </div>
                <FontAwesomeIcon
                    icon={faBookmark}
                    className="cursor-pointer"
                    size="lg"
                />
            </div>

            {/* Likes, name */}
            <div className="px-4">
                <p className="font-semibold mb-1 text-sm">354 Likes</p>
                <p className="mr-1 text-sm">
                    <span className="font-bold cursor-pointer">
                        DM_Wedding{" "}
                    </span>
                    Your love and support means a lot to us, and we thank you
                    for whatever contribution you choose.
                </p>
            </div>

            {/* Comments, view all comment */}
            {/* <div className="px-4 flex justify-between mt-2">
        <div className="text-sm">
          <span className="font-semibold mr-1">afifarifmustofa</span>
          Baru tau masmul bisa berpose di kamera😁
        </div>
        <FontAwesomeIcon icon={faHeart} className="h-4 cursor-pointer" />
      </div>
      <div className="px-4 flex justify-between">
        <div className="text-sm">
          <span className="font-semibold mr-1">_emirbandu22</span>
          wih, pake apa mas Stack undangannya?
        </div>
        <FontAwesomeIcon icon={faHeart} className="h-4 cursor-pointer" />
      </div> */}

            <div className="px-4 text-sm mb-1 text-gray-400 cursor-pointer">
                313 comments
            </div>

            {/* Time */}
            <div className="px-4 mt-2 text-gray-400 text-xs mb-4">
                2 DAYS AGO
            </div>
        </div>
    );
}
