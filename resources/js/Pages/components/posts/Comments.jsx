import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { router } from "@inertiajs/react";

export default function Comments({ props }) {
    // console.log(props);

    const [values, setValues] = useState({
        comment: "",
    });

    function handleChange(e) {
        const key = e.target.id;
        const value = e.target.value;
        setValues((values) => ({
            ...values,
            [key]: value,
        }));
    }

    function handleSubmit(e) {
        e.preventDefault();
        router.post(`/wedding/${props.name}`, values);
    }

    return (
        <div className="m-4 max-h-96 overflow-y-auto">
            <form onSubmit={handleSubmit} className="flex items-center pb-4">
                <input
                    id="comment"
                    type="text"
                    value={values.comment}
                    onChange={handleChange}
                    className="bg-slate-200 border-none flex-1 focus:ring-0 outline-none p-2"
                    placeholder="Add a comment..."
                    maxLength={255}
                    required
                />

                <button className="ml-2 font-semibold text-blue-400">
                    Post
                </button>
            </form>

            {/* Comments, view all comment */}
            {props.comments.map((comment) => (
                <div
                    key={comment.id}
                    className="px-4 flex justify-between pb-1"
                >
                    <div className="text-sm">
                        <span className="font-semibold mr-1">
                            {comment.people.name}
                        </span>
                        {comment.comment}
                    </div>
                    <FontAwesomeIcon
                        icon={faHeart}
                        className="h-4 cursor-pointer"
                        color="red"
                    />
                </div>
            ))}
        </div>
    );
}
