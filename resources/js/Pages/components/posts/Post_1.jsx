import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import {
    faPaperPlane,
    faHeart,
    faComment,
    faBookmark,
} from "@fortawesome/free-regular-svg-icons";

export default function Post_1() {
    return (
        <div className="bg-white my-4 border rounded-sm">
            {/* top, image profile, name, logo */}
            <div className="flex items-center p-3">
                <div className="avatar mr-3">
                    <div className="rounded-full w-10 h-10 cursor-pointer">
                        <img src="/images/feeds/feed 2.jpg" />
                    </div>
                </div>
                <p className="flex-1 text-sm font-semibold cursor-pointer">
                    DM_Wedding
                </p>
                <FontAwesomeIcon
                    icon={faEllipsisVertical}
                    className="h-5 mr-2 cursor-pointer"
                />
            </div>

            {/* images content */}
            <img className="w-full" src={"/images/feeds/feed 2.jpg"} />

            {/* logos */}
            <div className="flex justify-between p-4">
                <div className="flex space-x-4 items-center">
                    <FontAwesomeIcon
                        icon={faHeart}
                        className="cursor-pointer"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faComment}
                        className="cursor-pointer scale-x-[-1]"
                        size="lg"
                    />
                    <FontAwesomeIcon
                        icon={faPaperPlane}
                        className="cursor-pointer"
                        size="lg"
                    />
                </div>
                <FontAwesomeIcon
                    icon={faBookmark}
                    className="cursor-pointer"
                    size="lg"
                />
            </div>

            {/* Likes, name */}
            <div className="px-4">
                <p className="font-semibold mb-1 text-sm">354 Likes</p>
                <p className="mr-1 text-sm">
                    <span className="font-bold cursor-pointer">
                        DM_Wedding{" "}
                    </span>
                    Invitation of Denisa (Ica) & Mulki (Emul).
                </p>
            </div>

            {/* Comments, view all comment */}
            <div className="px-4 flex justify-between mt-2">
                <div className="text-sm">
                    <span className="font-semibold mr-1">mhafidzn31</span>
                    Akhirna. Semoga SAMAWA euy 😍😍
                </div>
                <FontAwesomeIcon
                    icon={faHeart}
                    className="h-4 cursor-pointer"
                />
            </div>
            <div className="px-4 flex justify-between">
                <div className="text-sm">
                    <span className="font-semibold mr-1">amii_204_Amrull</span>
                    Meni teu beja-beja..
                </div>
                <FontAwesomeIcon
                    icon={faHeart}
                    className="h-4 cursor-pointer"
                />
            </div>
            <div className="px-4 flex justify-between">
                <div className="text-sm">
                    <span className="font-semibold mr-1">azkanyeng</span>
                    Malem minggu jadi nikah, cakeep... (pantun)
                </div>
                <FontAwesomeIcon
                    icon={faHeart}
                    className="h-4 cursor-pointer"
                />
            </div>
            <div className="px-4 flex justify-between">
                <div className="text-sm">
                    <span className="font-semibold mr-1">indra_mf13</span>
                    Mugia Allah Paring ASLB mas mul..
                </div>
                <FontAwesomeIcon
                    icon={faHeart}
                    className="h-4 cursor-pointer"
                />
            </div>

            <div className="px-4 text-sm mb-1 text-gray-400 cursor-pointer">
                313 comments
            </div>

            {/* Time */}
            <div className="px-4 mt-2 text-gray-400 text-xs mb-4">
                2 DAYS AGO
            </div>
        </div>
    );
}
